'''
Created on 29 sty 2014
@author: Rafal
'''

import wx
import gettext

class MainWindow(wx.Frame):

    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds)
        
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        self.button_test = wx.Button(self, wx.ID_ANY, "Test")
        self.CreateWindow()

    def CreateWindow(self):
        app = wx.PySimpleApp()
        frame = wx.Frame(None, -1, "Czesc")
        
        frame.Show()
        app.MainLoop()
        
def main():
    print "MAIN"
    mw = MainWindow()

if __name__ == '__main__':
    main()
