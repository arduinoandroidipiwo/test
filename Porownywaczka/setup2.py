'''
Created on 24 sty 2014
@author: Rafal
'''

from distutils.core import setup
import py2exe, sys, os

sys.argv.append('py2exe')
# sys.argv.append('--bundle')
# sys.argv.append('1')


setup(console=['MainWindow2.py'],
#     options = {'py2exe':{"bundle_files":1,'compressed':True}},
    options = {'py2exe':{'compressed':True}},
    windows = [{'script': "MainWindow2.py"}],
    zipfile = None)
