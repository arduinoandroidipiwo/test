'''
Created on 29 sty 2014
@author: Rafal
'''

from struct import unpack, calcsize

def uintToInt(tab):
    return ord(tab[0]) + 256*ord(tab[1]) + 256*256*ord(tab[2]) + 256*256*256*ord(tab[3])

def ulongToInt(tab):
    return uintToInt(tab)

class Header(object):
    def __init__(self, tab):
        self.WERSJA = uintToInt(tab[0:4])
        self.TICK5s = ulongToInt(tab[4:8])
        self.OST_ZM = ulongToInt(tab[8:12])
        self.UPDATE = ulongToInt(tab[12:16])
        self.ROZM_EL = uintToInt(tab[16:20])
        self.POCZ_EL = uintToInt(tab[20:24])
        self.IL_ELEM = uintToInt(tab[24:28])

##############################################################
class Element(object):
    def __init__(self, tab):
        pass

##############################################################
class elPomiar(Element):
    def __init__(self, tab):
        Element.__init__(self, tab)
        self.POM = uintToInt(tab[0:4])  # POPRAWIC!!! ma byc ujemne!!!
#         print len(tab),
#         print ord(tab[4])
        self.STAT_OK = bool(ord(tab[4]) & 0x01)
        self.STAT_AL = bool(ord(tab[4]) & 0x02)
        self.STAT_OS = bool(ord(tab[4]) & 0x04)
        self.STAT_SK = bool(ord(tab[4]) & 0x10)

##############################################################
class elKarta(Element):
    def __init__(self, tab):
        Element.__init__(self, tab)
        
        t = unpack('=?18s6s2ifc6s6s6s6s6s6s81s81s81s2i6c4i', tab)
        
        self.AKTYWNA = t[0]
        self.nazwaT = t[1]
        self.jedn = t[2]
        self.min = t[3]
        self.max = t[4]
        self.div = t[5]
        self.fun = t[6]
        self.rej = t[7]
        self.tel = t[8]
        self.mazb = t[9]
        self.uwagi = t[10]
        self.rozdz = t[11]
        self.cz_podl = t[12]
        self.cz_zmia = t[13]
        self.warun1 = t[14]
        self.warun2 = t[15]
        self.usePA1 = t[16]
        self.usePO1 = t[17]
        self.usePA2 = t[18]
        self.usePO2 = t[19]
        self.PA1 = t[20]
        self.PO1 = t[21]
        self.PA2 = t[22]
        self.PO2 = t[23]
        
##############################################################
class ExtFile(object):

    def __init__(self):
        self.hdr = None
        self.elem = []
        
    def readPom(self, tab):
        self.hdr = Header(tab[0:28])
        self.elem = []
        for i in range(self.hdr.IL_ELEM):
            self.elem.append(elPomiar(tab[28+i*self.hdr.ROZM_EL:28+(i+1)*self.hdr.ROZM_EL]))
            
    def readKart(self, tab):
        self.hdr = Header(tab[0:28])
        self.elem = []
        for i in range(self.hdr.IL_ELEM):
            self.elem.append(elKarta(tab[28+i*self.hdr.ROZM_EL:28+(i+1)*self.hdr.ROZM_EL]))
        
##############################################################





