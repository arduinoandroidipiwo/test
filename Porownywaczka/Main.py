'''
Created on 24 sty 2014
@author: Rafal
'''

import hashlib
import time
import sys
import wx
import gettext

from MainWindow import MainWindow

def main():
       
#     filePath1 = "D:/Wymiana/extdta/DANE.EXT"
#     filePath2 = "D:/Wymiana/extdta/KARTY.EXT"
#       
#     logPath1 = "D:/Wymiana/extdta/log1.txt"
#     logPath2 = "D:/Wymiana/extdta/log2.txt"
    
    filePath1 = "../DANE.EXT"
    filePath2 = "../KARTY.EXT"
                
    logPath1 = "../logDane.txt"
    logPath2 = "../logKarty.txt"
    
    if len(sys.argv) >= 2:
        filePath1 = sys.argv[1]
    elif len(sys.argv) >= 3:
        filePath1 = sys.argv[1]
    
    fileContent1 = ""
    fileTime1 = 0
    fileContent2 = ""
    fileTime2 = 0
    
    while True:
        logFile1 = open(logPath1, 'a')
        logFile2 = open(logPath2, 'a')
        
        # Wczytanie pliku 1
        plik1 = open(filePath1, 'rb')
        entireFile1 = plik1.read()
        plik1.close()
        
        # Wczytanie pliku 2
        plik2 = open(filePath2, 'rb')
        entireFile2 = plik2.read()
        plik2.close()
        
        print hashlib.md5(entireFile1).hexdigest(),
        print ' ',
        print hashlib.md5(entireFile2).hexdigest()

        
        if entireFile1 != fileContent1:
            clk = time.time()
            logFile1.write(time.asctime(time.gmtime(clk)))
            logFile1.write(": ")
            logFile1.write(hashlib.md5(entireFile1).hexdigest())
            if fileTime1:
                logFile1.write("   --->  ")
                logFile1.write(str(round(clk - fileTime1, 1)))
                logFile1.write("s")
            logFile1.write('\n')
            
            fileContent1 = entireFile1
            fileTime1 = clk

        if entireFile2 != fileContent2:
            clk = time.time()
            logFile2.write(time.asctime(time.gmtime(clk)))
            logFile2.write(": ")
            logFile2.write(hashlib.md5(entireFile2).hexdigest())
            if fileTime2:
                logFile2.write("   --->  ")
                logFile2.write(str(round(clk - fileTime2, 1)))
                logFile2.write("s")
            logFile2.write('\n')
            
            fileContent2 = entireFile2
            fileTime2 = clk
        
        logFile1.close()
        logFile2.close()

        time.sleep(.2)

#------------------------------------------------

def test():
    print "Funkcja testowa"

def WinMain():
    print "WinMain"
    gettext.install("app") # replace with the appropriate catalog name
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    frame_1 = MainWindow(None, wx.ID_ANY, "")
    
#    frame_1.timer = wx.Timer(wx.EVT_TIMER, frame_1.update, frame_1.timer)
    
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()

#------------------------------------------------

if __name__ == '__main__':
    WinMain()
