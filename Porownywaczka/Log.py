'''
Created on 26 lut 2014
@author: Rafal
'''

import time

class Log(object):
    '''
    classdocs
    '''
    logFile = ""

    def __init__(self, logFile):
        self.logFile = logFile
        
    def write(self, txt, ts):
        now = time.localtime(time.time())
        f = open(self.logFile, "a")
        f.write("%04d-%02d-%02d %02d:%02d:%02d: %s" % (now[0], now[1], now[2], now[3], now[4], now[5], txt))

# -------------------------------------------
if __name__ == "__main__":
    l = Log("./log.txt")
    now = time.localtime(time.time())

