'''
Created on 25 lut 2014
@author: Rafal
'''

import re

class Settings(object):
    filename = ""
    params = {}

    def __init__(self, filename):
        self.filename = filename
        self.params = {}

    def addParam(self, name, param):
        self.params[name] = param

    def getParam(self, name):
        try:
            return self.params[name]
        except:
            return ""

    def save(self):
        fSet = open(self.filename, "w")
        for p in self.params:
            fSet.write("%s: %s\n" % (p, self.params[p]))
        fSet.close()

    def read(self):
        try:
            fSet = open(self.filename, 'r')
        except:
            fSet = None
            return None

        while True:
            line = fSet.readline()
            if line == "":
                break
            print line,
            result = re.match(r"^(\w*):\s+(.*)$", line)
            self.params[result.group(1)] = result.group(2)


# -------------------------------------------

if __name__ == "__main__":
    s = Settings("test.txt")
#     s.addParam("jeden", 1)
#     s.addParam("dwa", 2)
#     s.save()
    s.read()
    print s.params
